package com.bootdo.clouddoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


/**
 * @author 胡甫圣
 * @EnableEurekaServer 启动一个服务注册中心提供给其他应用进行对话
 *
 * 启动该服务后调用访问 http://localhost:8001/ 进入当前注册中心应用服务
 * Register(服务注册)：把自己的 IP 和端口注册给 Eureka。
 * Renew(服务续约)：发送心跳包，每 30 秒发送一次，告诉 Eureka 自己还活着。如果 90 秒还未发送心跳，宕机。
 * Cancel(服务下线)：当 Provider 关闭时会向 Eureka 发送消息，把自己从服务列表中删除，防止 Consumer 调用到不存在的服务。
 * Get Registry(获取服务注册列表)：获取其他服务列表。
 * Replicate(集群中数据同步)：Eureka 集群中的数据复制与同步。
 * Make Remote Call(远程调用)：完成服务的远程调用。
 * Renews threshold：Eureka Server 期望每分钟收到客户端实例续约的总数
 * Renews (last min)：Eureka Server 最后 1 分钟收到客户端实例续约的总数。
 */
@EnableEurekaServer
@SpringBootApplication
public class ClouddoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClouddoServerApplication.class, args);
	}
}
